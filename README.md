# PhaserKt

## What is PhaserKt?

PhaserKt is a set of KotlinJS bindings for the popular [Phaser](https://phaser.io/) JavaScript game engine. What
exactly does that mean? It means you can write Kotlin that
[compiles to JavaScript](https://kotlinlang.org/docs/reference/js-overview.html) that will then work with the Phaser
engine. Write Kotlin, target JavaScript and Phaser.

## How do I use it?

Being a KotlinJS library, ingesting PhaserKt into your application is a little...tricky. In short, you need to add a
dependency on the JavaScript side *and* on the Java side, and then keep those versions in sync.

**Please Note**: the version of `phaserkt` used in the following examples might be an older version than the one shown
in the above badge! Make sure you're using the latest version. And if you depend on `phaserkt` in multiple places, take
care to keep them in sync.

### Gradle

[![](https://jitpack.io/v/com.gitlab.nanodeath/phaserkt.svg)](https://jitpack.io/#com.gitlab.nanodeath/phaserkt)

If you're using Gradle, you need to do this:

```groovy
allprojects {
    repositories {
        maven { url 'https://jitpack.io' }
    }
}
dependencies {
    compile 'com.gitlab.nanodeath:phaserkt:0.0.3'
}
```

([see also](https://jitpack.io/#com.gitlab.nanodeath/phaserkt))

### package.json

[![npm version](https://badge.fury.io/js/phaserkt.svg)](https://badge.fury.io/js/phaserkt)

You also need to update your package.json:

```json
{
  "devDependencies": {
    "kotlin": "1.2.70",
    "phaser": "3.12.0",
    "phaserkt": "0.0.3"
  }
}

```

### Others

PhaserKt is hosted in Maven (using jitpack.io) and npm, as described above, so...you should be getting something from
each.

## How do I develop for it?

### Library Setup

1. Check out this repo.
2. Run `gradlew assembleDev` on Windows, or `./gradlew assembleDev` everywhere else.
3. Run `npm link`, assuming you're using npm. Not sure about yarn.
4. Repeat #2 every time you make a change.

### Game Setup

For npm, run `npm link phaserkt`.

For Gradle, add the following to your main build.gradle:

```groovy
repositories {
    mavenLocal()
}

dependencies {
    compile "name.nanodeath:phaserkt:1.0-SNAPSHOT"
    // comment out the existing phaserkt dependency you might have
}
```

Then...you're set. And yes, the version should always be `1.0-SNAPSHOT`.

### Commits and Releases

Structure your commits to follow [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0-beta.4/). **This
is critical.** Version number updates are based on these commit messages.

Releases and release notes will automatically be generated using [semantic-delivery-gitlab](https://www.npmjs.com/package/@hutson/semantic-delivery-gitlab).

## Versioning

You might have a couple questions:

* Which version of Phaser does this support?
* How do the versions of PhaserKt relate to the version of Phaser?
* What versioning scheme does PhaserKt itself try to follow? 

PhaserKt targets Phaser 3.

Phaser's API is...quite large. For a single person, or even a small group of people, porting that API over to Kotlin is
a substantial, drawn-out effort. That means it's fairly pointless to target a particular version of Phaser -- it's
simply moving too fast. So, on a best effort basis, until further notice, PhaserKt will always be targeting the *latest*
version of Phaser, whatever that may be. Once PhaserKt begins to approach full API coverage, we'll need to revisit this,
but we'll definitely settle on something saner than "only ever support the latest version"!

Prior to 1.0, and potentially after 1.0, PhaserKt strives to comply with
[semver 2.0.0](https://semver.org/spec/v2.0.0.html).



## Idealogical Stuff

### Goals

The aim of PhaserKt is to support every API that Phaser offers, but given that PhaserKt is written *by hand*, it will
often lag behind what's actually possible in Phaser.

Another goal is **documentation**. One of the biggest things holding Phaser back right now is good documentation --
many members are either lacking or have small errors, and few of the old tutorials for Phaser 2 have been updated for
Phaser 3. While we'll always be playing catch-up, when we port methods over we should also be porting over, and
improving upon, the documentation.

PhaserKt also aims to improve upon the APIs of Phaser in small ways here and there, mostly by use of
[extension methods](https://kotlinlang.org/docs/reference/extensions.html) and nothing fundamental.

Lastly, being a web game engine, we'd *like* to keep the generated code as fast and small as possible. This is a bit of
a challenging goal, given KotlinJS's generated output and standard lib, but anecdotally I've been able to get a "my
first Phaser game" tutorial in about 13kb of JS, minified and gzipped, excluding the Phaser library. Kotlin also emits
a moderate number of runtime checks (type and null checks mostly) that also hurt performance, but KotlinJS code can be
written in a way that minimizes that overhead.

### Why does PhaserKt exist?

Does this sound like you?

* You really really like Kotlin
  * And/or you don't like C# (Unity), Lua (LÖVE), JavaScript (...many), or C/++ (...even more)
* You don't like other JVM-based options
  * Such as JMonkeyEngine or LibGDX
  * Or you just don't want to target the JVM at all
* But you still want to write games

So given the Kotlin invariant, that leaves you with a few options:
* Write a NEW game engine in KotlinJS
* Write a NEW game engine in Kotlin Native
* Adapt an existing engine for KotlinJS
* Adapt an existing engine with Kotlin Native

Kotlin Native has a few strikes against it.
1. It's pre-1.0, so presumed unstable. Not really what you want in a language platform.
2. To really use it you need [JetBrains CLion](https://www.jetbrains.com/clion/), a PAID IDE. ~$100 USD/year!
3. Debugging errors means debugging native code. Not my cup of tea.

Writing a new game engine is a huge amount of work I don't have time for, so that's out.

Which leaves a single option: adapt an existing engine for KotlinJS.

Why Phaser? It's popular, has a lot of built-in features, is under active development, supports canvas and WebGL,
supports 2D and 3D, open source, etc etc.
 
### License

See [License](./LICENSE).

## Etc

### Why is PhaserKt written by hand? Can't you generate most of this stuff?

In theory, yes, but alas, that's all we've got right now.

*In theory*, we could take the [phaser.d.ts](https://github.com/photonstorm/phaser3-docs/blob/master/typescript/phaser.d.ts)
TypeScript definitions, run [ts2kt](https://github.com/Kotlin/ts2kt) on it, and presto, ready-to-use Kotlin bindings.
But ts2kt, to be blunt, just doesn't work that well. Not only are the generated types suboptimal, but they don't even
compile! [[1]](https://github.com/Kotlin/ts2kt/issues/83) [[2]](https://github.com/Kotlin/ts2kt/issues/84) I don't even
fully blame ts2kt, though -- Phaser makes obnoxiously heavy use of union types, which Kotlin
[doesn't support](https://discuss.kotlinlang.org/t/union-types/77/51) and thus ts2kt
[doesn't support](https://discuss.kotlinlang.org/t/ts2kt-union-intersection-types/7335).

Besides, that, fully automated bindings won't feel very Kotlin-y, so PhaserKt aims to make Phaser as easy to use from
Kotlin as possible by adding a little extra sugar and glue where it makes sense.

### How's the performance really?

For any project an individual or tiny group of developers undertakes, likely "good enough". But I have no idea if
KotlinJS for game development is actually a terrible idea. It's an adventure!

At worst, you can write your hot path code "natively" in JavaScript if performance is grievously bad.

### Is PhaserKt affiliated with Phaser?

No, it's not. It's an independent unblessed experimental and possibly ill-conceived venture.

### Other questions?

File an [issue](https://gitlab.com/nanodeath/phaserkt/issues/new), or shoot me a tweet
[![Twitter URL](https://img.shields.io/twitter/url/http/shields.io.svg?style=social)](https://twitter.com/nanodeath). 
