import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import sourcemaps from 'rollup-plugin-sourcemaps';

export default {
  input: 'build/classes/kotlin/main/phaserkt.js',
  output: {
    file: 'build/rollup/phaserkt.js',
    format: 'es',
    sourcemap: true
  },
  external: ["kotlin", "phaser"],
  plugins: [
    sourcemaps(),
    resolve({
        browser: true,
    }),
    commonjs()
  ]
};
