@file:JsModule("phaser")
@file:Suppress("unused")

package phaserkt

import phaserkt.animations.AnimationManager
import phaserkt.cameras.scene2d.CameraManager
import phaserkt.events.EventEmitter
import phaserkt.gameobjects.GameObjectCreator
import phaserkt.gameobjects.GameObjectFactory
import phaserkt.input.InputPlugin
import phaserkt.loader.LoaderPlugin
import phaserkt.physics.arcade.ArcadePhysics
import phaserkt.scale.ScaleManager
import phaserkt.scene.ScenePlugin
import phaserkt.time.Clock
import phaserkt.tweens.TweenManager

/**
 * [Docs](https://photonstorm.github.io/phaser3-docs/Phaser.phaserkt.Scene.html)
 */
open external class Scene(
        /** Can either be a [SettingsConfig] or a key, used in an otherwise empty config. */
        configOrKey: dynamic
) {
    val load: LoaderPlugin
    val add: GameObjectFactory
    val physics: ArcadePhysics
    val anims: AnimationManager
    val input: InputPlugin
    val time: Clock
    val cameras: CameraManager
    val make: GameObjectCreator
    val tweens: TweenManager
    val scale: ScaleManager
    val scene: ScenePlugin
    val events: EventEmitter
    val game: Game

    open fun init(data: dynamic)
    open fun preload(data: dynamic)
    open fun create(data: dynamic)
}
