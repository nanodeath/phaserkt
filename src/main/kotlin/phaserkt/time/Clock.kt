@file:JsModule("phaser")
@file:JsQualifier("Time")
@file:Suppress("unused")

package phaserkt.time

external class Clock {
    val now: Int
}
