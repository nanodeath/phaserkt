@file:JsModule("phaser")
@file:JsQualifier("Physics.Arcade")
@file:Suppress("unused")

package phaserkt.physics.arcade

external class Factory {
    fun <T> staticGroup(children: dynamic = definedExternally, config: dynamic = definedExternally): StaticGroup<T>
    fun <T> group(children: dynamic = definedExternally, config: dynamic = definedExternally): Group<T>
    fun sprite(x: Float, y: Float, key: String, frame: dynamic = definedExternally): ArcadeSprite
    fun sprite(x: Int, y: Int, key: String, frame: dynamic = definedExternally): ArcadeSprite
    fun collider(
        object1: dynamic,
        object2: dynamic,
        collideCallback: ArcadePhysicsCallback = definedExternally,
        processCallback: ArcadePhysicsCallback = definedExternally,
        callbackContext: dynamic = definedExternally
    ): dynamic /* Collider */

    fun overlap(
        object1: dynamic,
        object2: dynamic,
        collideCallback: ArcadePhysicsCallback = definedExternally,
        processCallback: ArcadePhysicsCallback = definedExternally,
        callbackContext: dynamic = definedExternally
    ): dynamic /* Collider */
}
