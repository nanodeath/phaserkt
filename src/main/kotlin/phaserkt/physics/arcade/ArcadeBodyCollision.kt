@file:JsModule("phaser")
@file:JsQualifier("Physics.Arcade")
@file:Suppress("unused")

package phaserkt.physics.arcade

external class ArcadeBodyCollision {
    val none: Boolean
    val up: Boolean
    val down: Boolean
    val left: Boolean
    val right: Boolean
}
