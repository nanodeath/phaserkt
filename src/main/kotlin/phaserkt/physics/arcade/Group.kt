@file:JsModule("phaser")
@file:JsQualifier("Physics.Arcade")
@file:Suppress("unused")

package phaserkt.physics.arcade

import phaserkt.structs.PhaserSet

external class Group<T> {
    /**
     * Count the number of active elements of this group, unless [value] is false, and then count inactive elements.
     */
    fun countActive(value: Boolean = definedExternally): Int

    fun create(
        x: Float = definedExternally, y: Float = definedExternally, key: String = definedExternally,
        frame: dynamic = definedExternally, visible: Boolean = definedExternally,
        active: Boolean = definedExternally
    ): T

    val children: PhaserSet<T>
}
