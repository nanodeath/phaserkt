@file:Suppress("FunctionName", "unused")

package phaserkt.physics.arcade

import phaserkt.jsApply

external interface PhysicsGroupConfig {
    var immovable: Boolean? get() = definedExternally; set(value) = definedExternally
    var velocityX: Float? get() = definedExternally; set(value) = definedExternally
    var frictionX: Float? get() = definedExternally; set(value) = definedExternally
    var allowGravity: Boolean? get() = definedExternally; set(value) = definedExternally
}

fun PhysicsGroupConfig(cb: (PhysicsGroupConfig.() -> Unit) = {}) = jsApply(cb)
