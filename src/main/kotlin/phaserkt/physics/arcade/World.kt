@file:JsModule("phaser")
@file:JsQualifier("Physics.Arcade")
@file:Suppress("unused")

package phaserkt.physics.arcade

import phaserkt.gameobjects.Graphics

external class World {
    fun createDebugGraphic(): Graphics
}
