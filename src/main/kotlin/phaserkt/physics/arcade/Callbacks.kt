package phaserkt.physics.arcade

import phaserkt.gameobjects.GameObject

typealias ArcadePhysicsCallback = (object1: GameObject, object2: GameObject) -> Unit
