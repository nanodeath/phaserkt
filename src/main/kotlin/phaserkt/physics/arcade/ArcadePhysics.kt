@file:JsModule("phaser")
@file:JsQualifier("Physics.Arcade")
@file:Suppress("unused")

package phaserkt.physics.arcade

external class ArcadePhysics {
    val add: Factory
    val world: World
    fun pause(): World
    fun resume(): World
}
