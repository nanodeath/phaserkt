@file:JsModule("phaser")
@file:JsQualifier("Physics.Arcade")
@file:Suppress("unused")

package phaserkt.physics.arcade

import phaserkt.gameobjects.components.Gravity
import phaserkt.gameobjects.components.Size
import phaserkt.gameobjects.components.TextureCrop
import phaserkt.gameobjects.components.Transform
import phaserkt.textures.Frame

@JsName("Sprite")
external class ArcadeSprite : phaserkt.gameobjects.Sprite, Transform, Gravity, Size,
    phaserkt.physics.arcade.components.Size, TextureCrop {
    fun setBounce(x: Float, y: Float = definedExternally): ArcadeSprite
    fun setBounceY(y: Float): ArcadeSprite
    fun setCollideWorldBounds(value: Boolean): ArcadeSprite

    fun setVelocity(x: Float, y: Float = definedExternally): ArcadeSprite
    fun setVelocityX(x: Float): ArcadeSprite
    fun setVelocityY(y: Float): ArcadeSprite

    fun setFriction(x: Float, y: Float = definedExternally): ArcadeSprite
    fun setFrictionX(x: Float): ArcadeSprite
    fun setFrictionY(y: Float): ArcadeSprite

    val body: Body?
    fun refreshBody(): ArcadeSprite
    fun enableBody(reset: Boolean, x: Float, y: Float, enableGameObject: Boolean, showGameObject: Boolean): ArcadeSprite
    fun disableBody(
        disableGameObject: Boolean = definedExternally,
        hideGameObject: Boolean = definedExternally
    ): ArcadeSprite

    // GameObjects.Transform //

    override var x: Float = definedExternally
    override var y: Float = definedExternally
    override var z: Float = definedExternally
    override var w: Float = definedExternally
    override var scaleX: Float = definedExternally
    override var scaleY: Float = definedExternally
    override var angle: Float = definedExternally
    override var rotation: Float = definedExternally
    override fun setPosition(x: Float, y: Float, z: Float, w: Float) = definedExternally
    override fun setRandomPosition(x: Float, y: Float, width: Float, height: Float) = definedExternally
    override fun getLocalTransformMatrix(tempMatrix: dynamic): dynamic = definedExternally
    override fun getWorldTransformMatrix(tempMatrix: dynamic, parentMatrix: dynamic): dynamic = definedExternally

    // GameObjects.Gravity //

    override fun setGravity(x: Float, y: Float): ArcadeSprite = definedExternally
    override fun setGravityX(x: Float): ArcadeSprite = definedExternally
    override fun setGravityY(y: Float): ArcadeSprite = definedExternally

    // GameObjects.Size //

    override val width: Float = definedExternally
    override val height: Float = definedExternally
    override var displayWidth: Float = definedExternally
    override var displayHeight: Float = definedExternally

    override fun setSizeToFrame(frame: Frame): Size = definedExternally
    /** NOT IMPLEMENTED. */
    override fun setSize(width: Float, height: Float): ArcadeSprite = definedExternally

    override fun setDisplaySize(width: Float, height: Float): ArcadeSprite = definedExternally

    // Arcade.Size //

    override fun setOffset(x: Float, y: Float): ArcadeSprite = definedExternally
    /**
     * Sizes and positions this Body's boundary, as a rectangle.
     * Modifies the Body `offset` if `center` is true (the default).
     * Resets the width and height to match current frame, if no width and height provided and a frame is found.
     */
    override fun setSize(width: Float, y: Float, center: Boolean): ArcadeSprite = definedExternally

    override fun setCircle(radius: Float, offsetX: Float, offsetY: Float): ArcadeSprite = definedExternally

    // GameObjects.TextureCrop //

    override fun setTexture(key: String, frame: dynamic): TextureCrop = definedExternally
}
