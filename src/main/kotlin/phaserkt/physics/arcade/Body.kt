@file:JsModule("phaser")
@file:JsQualifier("Physics.Arcade")
@file:Suppress("unused")

package phaserkt.physics.arcade

import phaserkt.math.Vector2

external class Body {
    var allowDrag: Boolean
    var allowGravity: Boolean
    var allowRotation: Boolean
    /** Whether this Body is colliding with another and in which direction. */

    val touching: ArcadeBodyCollision
    /** Whether this Body is colliding with a tile or the world boundary. */
    val blocked: ArcadeBodyCollision
    var velocity: Vector2

    /**
     * Sets speed in pixels/second.
     */
    fun setVelocity(x: Float, y: Float = definedExternally): Body

    fun setVelocityX(value: Float): Body
    fun setVelocityY(value: Float): Body
}
