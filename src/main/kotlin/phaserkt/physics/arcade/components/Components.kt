@file:JsModule("phaser")
@file:JsQualifier("Physics.Arcade.Components")

package phaserkt.physics.arcade.components

external interface Size {
    fun setOffset(x: Float, y: Float = definedExternally): Size
    fun setSize(width: Float, y: Float, center: Boolean = definedExternally): Size
    fun setCircle(radius: Float, offsetX: Float, offsetY: Float): Size
}
