@file:JsModule("phaser")
@file:JsQualifier("Cameras.Scene2D")
@file:Suppress("unused", "MemberVisibilityCanBePrivate")

package phaserkt.cameras.scene2d

import phaserkt.gameobjects.GameObject

external class Camera {
    var scrollX: Float
    var scrollY: Float

    val centerX: Float
    val centerY: Float

    fun startFollow(
            target: GameObject,
            roundPixels: Boolean = definedExternally,
            lerpX: Float = definedExternally,
            lerpY: Float = definedExternally,
            offsetX: Float = definedExternally,
            offsetY: Float = definedExternally
    ): Camera

    fun stopFollow(): Camera

    /**
     * Set the bounds of the [Camera]. The bounds are an axis-aligned rectangle.
     *
     * See [setBounds](https://photonstorm.github.io/phaser3-docs/Phaser.phaserkt.phaserkt.cameras.scene2d.Camera.html#setBounds__anchor).
     */
    fun setBounds(
            /** The top-left x coordinate of the bounds. */
            x: Int,
            /** The top-left y coordinate of the bounds. */
            y: Int,
            /** The width of the bounds, in pixels. */
            width: Int,
            /** The height of the bounds, in pixels. */
            height: Int,
            /** If true the [Camera] will automatically be centered on the new bounds. */
            centerOn: Boolean = definedExternally
    ): Camera

    /**
     * Set the position of the Camera viewport within the game.
     *
     * This does not change where the camera is 'looking'. See [setScroll] to control that.
     *
     * @param x The top-left x coordinate of the Camera viewport.
     * @param y The top-left y coordinate of the Camera viewport.
     */
    fun setPosition(
            x: Float,
            y: Float = definedExternally): Camera

    /**
     * Set the position of where the Camera is looking within the game. You can also modify the properties
     * [scrollX] and [scrollY] directly. Use this method, or the scroll properties, to move your camera around the game
     * world.
     *
     * This does not change where the camera viewport is placed. See [setPosition] to control that.
     *
     * @param x The x coordinate of the Camera in the game world.
     * @param y The y coordinate of the Camera in the game world.
     */
    fun setScroll(
            x: Float,
            y: Float = definedExternally): Camera
}
