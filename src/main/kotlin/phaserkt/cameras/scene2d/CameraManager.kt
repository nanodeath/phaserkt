@file:JsModule("phaser")
@file:JsQualifier("Cameras.Scene2D")
@file:Suppress("unused")

package phaserkt.cameras.scene2d

external class CameraManager {
    val cameras: Array<Camera>
    val main: Camera
}
