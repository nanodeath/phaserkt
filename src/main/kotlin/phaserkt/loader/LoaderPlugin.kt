@file:JsModule("phaser")
@file:JsQualifier("Loader")

package phaserkt.loader

// https://photonstorm.github.io/phaser3-docs/Phaser.Loader.LoaderPlugin.html
external class LoaderPlugin {
    fun image(key: String, url: String)
    fun spritesheet(key: String, url: String? = definedExternally, frameConfig: dynamic = definedExternally, xhrSettings: dynamic = definedExternally)
    /**
     * Adds a Tiled JSON Tilemap file, or array of map files, to the current load queue.
     *
     * See [phaserkt.loader.LoaderPlugin](https://photonstorm.github.io/phaser3-docs/Phaser.Loader.phaserkt.loader.LoaderPlugin.html#tilemapTiledJSON__anchor).
     */
    fun tilemapTiledJSON(key: dynamic, url: String = definedExternally, xhrSettings: dynamic = definedExternally): LoaderPlugin
}
