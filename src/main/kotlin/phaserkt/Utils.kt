@file:Suppress("unused")

package phaserkt

/** Execute the given 0-argument callback with the 'this context' set to [T]. */
inline fun <reified T> withThis(crossinline callback: T.() -> Unit): () -> Unit = { callback(js("this") as T) }

/** Execute the given 1-argument callback with the 'this context' set to [T]. */
inline fun <reified T, A> withThis1(crossinline callback: T.(a: A) -> Unit): (a: A) -> Unit =
    { a: A -> callback(js("this") as T, a) }

/** Execute the given 2-argument callback with the 'this context' set to [T]. */
inline fun <reified T, A, B> withThis2(crossinline callback: T.(a: A, b: B) -> Unit): (a: A, b: B) -> Unit =
    { a: A, b: B -> callback(js("this") as T, a, b) }

/**
 * Calls the given [cb] initializer on a new {}-style object.
 */
inline fun <T> jsLet(cb: (T) -> Unit): T = jsLet(js("{}"), cb)

/**
 * Calls the given [cb] initializer on a new {}-style object.
 */
inline fun <T> jsApply(cb: T.() -> Unit): T = jsApply(js("{}"), cb)

/**
 * Calls the given [cb] initializer on [init].
 */
inline fun <T> jsLet(init: dynamic, cb: (T) -> Unit): T {
    cb(init.unsafeCast<T>())
    return init.unsafeCast<T>()
}

/**
 * Calls the given [cb] initializer on [init].
 */
inline fun <T> jsApply(init: dynamic, cb: T.() -> Unit): T {
    val memoAsT = init.unsafeCast<T>()
    cb(memoAsT)
    return memoAsT
}

/**
 * Calls the given [cb] on [init], but dynamically -- you can attach any property you like to the object.
 */
inline fun <T> jsApplyDynamic(init: dynamic, cb: dynamic.() -> Unit): T = jsApply(init, cb) as T

/**
 * Calls the given [cb] initializer on this, but treat this as a [T] for the duration of the callback.
 *
 * @param R the initial and return type you're mutating
 * @param T the type to treat [init] as during callback execution
 * @return this
 */
inline fun <R, T> R.jsApplyAs(cb: T.() -> Unit): R {
    cb(this.unsafeCast<T>())
    return this
}

/** Construct a JavaScript array from the given [elements]. */
@Suppress("NOTHING_TO_INLINE")
inline fun jsArray(vararg elements: dynamic): dynamic = elements
