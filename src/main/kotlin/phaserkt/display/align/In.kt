@file:JsModule("phaser")
@file:JsQualifier("Display.Align.In")
@file:Suppress("unused")

package phaserkt.display.align

import phaserkt.gameobjects.GameObject

external class BottomLeft(gameObject: GameObject, alignIn: GameObject, offsetX: Float = definedExternally, offsetY: Float = definedExternally) : GameObject
external class BottomCenter(gameObject: GameObject, alignIn: GameObject, offsetX: Float = definedExternally, offsetY: Float = definedExternally) : GameObject
external class BottomRight(gameObject: GameObject, alignIn: GameObject, offsetX: Float = definedExternally, offsetY: Float = definedExternally) : GameObject

external class LeftCenter(gameObject: GameObject, alignIn: GameObject, offsetX: Float = definedExternally, offsetY: Float = definedExternally) : GameObject
external class Center(gameObject: GameObject, alignIn: GameObject, offsetX: Float = definedExternally, offsetY: Float = definedExternally) : GameObject
external class RightCenter(gameObject: GameObject, alignIn: GameObject, offsetX: Float = definedExternally, offsetY: Float = definedExternally) : GameObject

external class TopLeft(gameObject: GameObject, alignIn: GameObject, offsetX: Float = definedExternally, offsetY: Float = definedExternally) : GameObject
external class TopCenter(gameObject: GameObject, alignIn: GameObject, offsetX: Float = definedExternally, offsetY: Float = definedExternally) : GameObject
external class TopRight(gameObject: GameObject, alignIn: GameObject, offsetX: Float = definedExternally, offsetY: Float = definedExternally) : GameObject

external class QuickSet(child: GameObject, alignIn: GameObject, position: Int, offsetX: Float = definedExternally, offsetY: Float = definedExternally) : GameObject