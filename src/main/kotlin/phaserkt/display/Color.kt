@file:JsModule("phaser")
@file:JsQualifier("Display")

package phaserkt.display

/**
 * [Source](https://github.com/photonstorm/phaser/blob/v3.12.0/src/display/color/Color.js).
 */
external class Color(
    red: Int = definedExternally,
    green: Int = definedExternally,
    blue: Int = definedExternally,
    alpha: Int = definedExternally
) {
    fun transparent(): Color
    fun setTo(red: Int, green: Int, blue: Int, alpha: Int = definedExternally): Color
    fun setGLTo(red: Float, green: Float, blue: Float, alpha: Float = definedExternally): Color
    fun setFromRGB(color: dynamic): Color

    fun update(): Color
    fun clone(): Color

    val color: Int
    val color32: Int
    val rgba: String

    var redGL: Float
    var greenGL: Float
    var blueGL: Float
    var alphaGL: Float

    var red: Int
    var green: Int
    var blue: Int
    var alpha: Int
}
