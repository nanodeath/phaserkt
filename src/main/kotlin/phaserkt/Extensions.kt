@file:Suppress("unused")

package phaserkt

import phaserkt.gameobjects.components.ScrollFactor
import phaserkt.physics.arcade.PhysicsGroupConfig

@Suppress("UnsafeCastFromDynamic")
fun physicsGroupConfig(cb: PhysicsGroupConfig.() -> Unit): PhysicsGroupConfig {
    val obj = js("{}")
    cb(obj)
    return obj
}

// Like fixedToCamera = true from Phaser 2.
fun ScrollFactor.attachToCamera(): ScrollFactor = setScrollFactor(0F)
