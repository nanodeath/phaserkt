@file:JsModule("phaser")
@file:Suppress("unused")

package phaserkt

import phaserkt.loader.LoaderPlugin
import phaserkt.scale.ScaleManager
import phaserkt.scene.SceneManager

external class Game(config: GameConfig = definedExternally) {
    val load: LoaderPlugin
    val scale: ScaleManager
    var scene: SceneManager
}
