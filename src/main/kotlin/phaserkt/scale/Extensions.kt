package phaserkt.scale

import phaserkt.structs.Size

fun ScaleManager.onResize(cb: (gameSize: Size, baseSize: Size, displaySize: Size, resolution: Float, previousWidth: Int, previousHeight: Int) -> Unit) {
    on(Events.RESIZE, cb)
}