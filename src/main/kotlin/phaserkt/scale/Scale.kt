@file:Suppress("unused")
@file:JsModule("phaser")

package phaserkt.scale

external object Scale {

    // Scale modes
    // https://github.com/photonstorm/phaser/blob/e0ec646496fbc8a6083f8ef88813dd9a90233150/src/scale/const/SCALE_MODE_CONST.js

    /**
     * No scaling happens at all. The canvas is set to the size given in the game config and Phaser doesn't change it
     * again from that point on. If you change the canvas size, either via CSS, or directly via code, then you need
     * to call the Scale Managers `resize` method to give the new dimensions, or input events will stop working.
     *
     * @name Phaser.Scale.ScaleModes.NONE
     * @type {integer}
     * @const
     * @since 3.16.0
     */
    val NONE: ScaleModeType = definedExternally
    /**
     * The height is automatically adjusted based on the width.
     *
     * @name Phaser.Scale.ScaleModes.WIDTH_CONTROLS_HEIGHT
     * @type {integer}
     * @const
     * @since 3.16.0
     */
    val WIDTH_CONTROLS_HEIGHT: ScaleModeType = definedExternally
    /**
     * The width is automatically adjusted based on the height.
     *
     * @name Phaser.Scale.ScaleModes.HEIGHT_CONTROLS_WIDTH
     * @type {integer}
     * @const
     * @since 3.16.0
     */
    val HEIGHT_CONTROLS_WIDTH: ScaleModeType = definedExternally
    /**
     * The width and height are automatically adjusted to fit inside the given target area,
     * while keeping the aspect ratio. Depending on the aspect ratio there may be some space
     * inside the area which is not covered.
     *
     * @name Phaser.Scale.ScaleModes.FIT
     * @type {integer}
     * @const
     * @since 3.16.0
     */
    val FIT: ScaleModeType = definedExternally
    /**
     * The width and height are automatically adjusted to make the size cover the entire target
     * area while keeping the aspect ratio. This may extend further out than the target size.
     *
     * @name Phaser.Scale.ScaleModes.ENVELOP
     * @type {integer}
     * @const
     * @since 3.16.0
     */
    val ENVELOP: ScaleModeType = definedExternally
    /**
     * The Canvas is resized to fit all available _parent_ space, regardless of aspect ratio.
     *
     * @name Phaser.Scale.ScaleModes.RESIZE
     * @type {integer}
     * @const
     * @since 3.16.0
     */
    val RESIZE: ScaleModeType = definedExternally

    // Center modes
    // https://github.com/photonstorm/phaser/blob/e0ec646496fbc8a6083f8ef88813dd9a90233150/src/scale/const/CENTER_CONST.js
    val NO_CENTER: CenterModeType = definedExternally
    val CENTER_BOTH: CenterModeType = definedExternally
    val CENTER_HORIZONTALLY: CenterModeType = definedExternally
    val CENTER_VERTICALLY: CenterModeType = definedExternally
}

external class ScaleModeType
external class CenterModeType