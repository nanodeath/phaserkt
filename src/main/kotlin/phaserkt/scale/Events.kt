@file:JsModule("phaser")
@file:JsQualifier("Scale")

package phaserkt.scale

external object Events {
    val RESIZE: ScaleEvent = definedExternally
}

external interface ScaleEvent