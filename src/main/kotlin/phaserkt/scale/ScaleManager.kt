@file:JsModule("phaser")
@file:JsQualifier("Scale")
@file:Suppress("unused")

package phaserkt.scale

import phaserkt.events.EventEmitter

external class ScaleManager : EventEmitter {
    val isFullscreen: Boolean = definedExternally
    fun startFullscreen(
            /**
             * https://developer.mozilla.org/en-US/docs/Web/API/FullscreenOptions
             */
            fullscreenOptions: dynamic = definedExternally
    )
    fun stopFullscreen()
    fun toggleFullscreen(
            /**
             * @see https://developer.mozilla.org/en-US/docs/Web/API/FullscreenOptions
             */
            fullscreenOptions: dynamic = definedExternally
    )
}

