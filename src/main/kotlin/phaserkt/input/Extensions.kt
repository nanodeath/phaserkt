@file:Suppress("unused")

package phaserkt.input

import org.w3c.dom.events.Event
import org.w3c.dom.events.KeyboardEvent
import phaserkt.events.EventEmitter
import phaserkt.gameobjects.GameObject

fun EventEmitter.onPointerDown(fn: (pointer: Pointer, gameObject: GameObject) -> Unit) = on("pointerdown", fn)

fun EventEmitter.onPointerUp(fn: (pointer: Pointer, gameObject: GameObject) -> Unit) = on("pointerup", fn)

fun InputPlugin.onKeyDown(fn: (event: KeyboardEvent) -> Unit) = keyboard.on("keydown") { ev: Event -> fn(ev as KeyboardEvent) }

fun InputPlugin.onKeyUp(fn: (event: KeyboardEvent) -> Unit) = keyboard.on("keyup") { ev: Event -> fn(ev as KeyboardEvent) }
