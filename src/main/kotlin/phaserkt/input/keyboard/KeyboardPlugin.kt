@file:JsModule("phaser")
@file:JsQualifier("Input.Keyboard")

package phaserkt.input.keyboard

import org.w3c.dom.events.Event

external class KeyboardPlugin {
    fun createCursorKeys(): CursorKeys
    fun once(event: String, fn: (Event) -> Unit)
    fun on(event: String, fn: (Event) -> Unit)
}
