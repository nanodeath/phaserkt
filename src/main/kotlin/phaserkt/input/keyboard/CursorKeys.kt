@file:JsModule("phaser")
@file:JsQualifier("Input.Keyboard")

package phaserkt.input.keyboard

external class CursorKeys {
    val up: Key
    val down: Key
    val left: Key
    val right: Key
    val space: Key
    val shift: Key
}

external class Key(keyCode: Int) {
    val isDown: Boolean
    val isUp: Boolean
}
