@file:JsModule("phaser")
@file:JsQualifier("Input")
@file:Suppress("unused")

package phaserkt.input

external class Pointer {
    var x: Float
    var y: Float
    var downX: Float
    var downY: Float
    var upX: Float
    var upY: Float
    fun getDistance(): Float
    fun getDistanceX(): Float
    fun getDistanceY(): Float
}