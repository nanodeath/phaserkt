@file:JsModule("phaser")
@file:JsQualifier("Input")
@file:Suppress("unused")

package phaserkt.input

import phaserkt.events.EventEmitter
import phaserkt.input.keyboard.KeyboardPlugin

external class InputPlugin : EventEmitter {
    val keyboard: KeyboardPlugin
}