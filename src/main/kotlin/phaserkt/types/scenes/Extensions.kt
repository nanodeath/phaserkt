@file:Suppress("unused", "FunctionName")

package phaserkt.types.scenes

import phaserkt.jsApply

fun SettingsConfig(cb: (SettingsConfig.() -> Unit) = {}) = jsApply(cb)