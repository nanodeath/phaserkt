@file:JsModule("phaser")
@file:JsQualifier("Types.Scenes")
@file:Suppress("unused")

package phaserkt.types.scenes

external object SettingsConfig {
    var key: String
    var active: Boolean
    var visible: Boolean
}
