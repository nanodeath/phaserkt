package phaserkt.types.gameobjects.text

/**
 * https://github.com/photonstorm/phaser/blob/e0ec646496fbc8a6083f8ef88813dd9a90233150/src/gameobjects/text/typedefs/TextStyle.js
 */
external class TextStyle {
    var fontFamily: String
    var fontSize: String
    var fontStyle: String
    var backgroundColor: String
    var color: String
    var stroke: String
    var strokeThickness: Float
    var shadow: dynamic
    var padding: dynamic
    var align: String
    var maxLines: Int
    var fixedWith: Float
    var fixedHeight: Float
    var resolution: Float
    var rtl: Boolean
    var testString: String
    var baselineX: String
    var baselineY: String
    var wordWrap: dynamic
    var metrics: dynamic
}