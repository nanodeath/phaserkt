@file:Suppress("unused")

package phaserkt.types.gameobjects.text

import phaserkt.jsApply

fun TextStyle(cb: (TextStyle.() -> Unit) = {}) = jsApply(cb)