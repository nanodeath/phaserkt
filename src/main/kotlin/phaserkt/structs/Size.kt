package phaserkt.structs

import phaserkt.math.Vector2

external class Size {
    val aspectMode: Int
    val aspectRatio: Float
    var height: Float
    val maxHeight: Int
    val maxWidth: Int
    val minHeight: Int
    val minWidth: Int
    val snapTo: Vector2
    val width: Float
}