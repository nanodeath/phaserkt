package phaserkt.structs

inline fun <T> PhaserSet<T>.forEach(crossinline cb: (T) -> Unit) = entries.forEach(cb)
inline operator fun <T> PhaserSet<T>.iterator() = entries.iterator()
inline fun <T> PhaserSet<T>.asSequence(): Sequence<T> = entries.asSequence()
inline fun <T> PhaserSet<T>?.orEmpty(): PhaserSet<T> = this ?: PhaserSet()
