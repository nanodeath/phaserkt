@file:JsModule("phaser")
@file:JsQualifier("Structs")
@file:Suppress("unused")

package phaserkt.structs

@JsName("Set")
external class PhaserSet<T> {
    fun iterate(callback: (entry: T, index: Int) -> Unit, callbackScope: dynamic = definedExternally): PhaserSet<T>
    val entries: Array<T>
}
