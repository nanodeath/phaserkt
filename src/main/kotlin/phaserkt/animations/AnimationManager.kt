@file:JsModule("phaser")
@file:JsQualifier("Animations")
@file:Suppress("PackageName", "unused")

package phaserkt.animations

/**
Animations are managed by the global Animation Manager. This is a singleton class that is
responsible for creating and delivering animations and their corresponding data to all Game Objects.
Unlike plugins it is owned by the Game instance, not the phaserkt.Scene.

[Docs](https://photonstorm.github.io/phaser3-docs/Phaser.Animations.AnimationManager.html).
 */
external class AnimationManager {
    /**
     * Constructs a new [Animation] based on the provided [config].
     */
    fun create(config: AnimationConfig): Animation

    /**
     * Generates an array of [AnimationFrameConfig] objects from a texture key and configuration object.
     *
     * Generates frames based on integers -- [GenerateFrameNamesConfig.frames], or if that's not given,
     * [GenerateFrameNamesConfig.start] to [GenerateFrameNamesConfig.end] (inclusive).
     */
    fun generateFrameNumbers(
        /** The key for the texture containing the animation frames. */
        key: String,
        /** The configuration object for the animation frames. */
        config: GenerateFrameNumbersConfig = definedExternally
    ): Array<AnimationFrameConfig>

    /**
     * Generates an array of [AnimationFrameConfig] objects from a texture key and configuration object.
     *
     * Generates frames based on names -- [GenerateFrameNamesConfig.frames] (prefixed with [GenerateFrameNamesConfig.prefix],
     * then optionally [GenerateFrameNamesConfig.zeroPad] number of zeros, then followed up with [GenerateFrameNamesConfig.suffix]),
     * or if that's not given, [GenerateFrameNamesConfig.start] to [GenerateFrameNamesConfig.end] (inclusive), with the
     * same prefix/zeroPad/suffix treatment.
     */
    fun generateFrameNames(
        key: String,
        config: GenerateFrameNamesConfig = definedExternally
    ): Array<AnimationFrameConfig>
}
