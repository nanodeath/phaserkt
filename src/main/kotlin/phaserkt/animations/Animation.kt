@file:JsModule("phaser")
@file:JsQualifier("Animations")

package phaserkt.animations

/**
A Frame based Animation.

This consists of a key, some default values (like the frame rate) and a bunch of Frame objects.

[Docs](https://photonstorm.github.io/phaser3-docs/Phaser.Animations.Animation.html)
 */
external class Animation {
}
