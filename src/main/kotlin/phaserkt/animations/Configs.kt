@file:Suppress("unused", "PackageName", "FunctionName")

package phaserkt.animations

import phaserkt.jsApply

external interface AnimationFrameConfig {
    var key: String? get() = definedExternally; set(value) = definedExternally
    var frame: dynamic get() = definedExternally; set(value) = definedExternally
    var duration: Float? get() = definedExternally; set(value) = definedExternally
    var visible: Boolean? get() = definedExternally; set(value) = definedExternally
}

fun AnimationFrameConfig(cb: (AnimationFrameConfig.() -> Unit) = {}) = jsApply(cb)

/**
 * [Docs](https://photonstorm.github.io/phaser3-docs/global.html#AnimationConfig).
 */
external interface AnimationConfig {
    var key: String? get() = definedExternally; set(value) = definedExternally
    var frames: Array<AnimationFrameConfig>? get() = definedExternally; set(value) = definedExternally
    var defaultTextureKey: String? get() = definedExternally; set(value) = definedExternally
    var frameRate: Int? get() = definedExternally; set(value) = definedExternally
    var duration: Int? get() = definedExternally; set(value) = definedExternally
    var skipMissedFrames: Boolean? get() = definedExternally; set(value) = definedExternally
    var delay: Int? get() = definedExternally; set(value) = definedExternally
    var repeat: Int? get() = definedExternally; set(value) = definedExternally
    var repeatDelay: Int? get() = definedExternally; set(value) = definedExternally
    var yoyo: Boolean? get() = definedExternally; set(value) = definedExternally
    var showOnStart: Boolean? get() = definedExternally; set(value) = definedExternally
    var hideOnComplete: Boolean? get() = definedExternally; set(value) = definedExternally
}

fun AnimationConfig(cb: (AnimationConfig.() -> Unit) = {}) = jsApply(cb)

external interface GenerateFrameNumbersConfig {
    /** The starting frame of the animation. */
    var start: Int?
    /** The ending frame of the animation. */
    var end: Int?
    /**
     * A frame to put at the beginning of the animation, before `start` or `outputArray` or `frames`.
     * phaserkt.Type: [Boolean] or [Int].
     */
    var first: dynamic
    /** An array to concatenate the output onto. */
    var outputArray: Array<AnimationFrameConfig>? get() = definedExternally; set(value) = definedExternally
    /**
     * A custom sequence of frames.
     * phaserkt.Type: [Boolean] or [IntArray].
     */
    var frames: IntArray?
}

fun GenerateFrameNumbersConfig(cb: (GenerateFrameNumbersConfig.() -> Unit) = {}) = jsApply(cb)

external interface GenerateFrameNamesConfig {
    var prefix: String? get() = definedExternally; set(value) = definedExternally
    var start: Int? get() = definedExternally; set(value) = definedExternally
    var end: Int? get() = definedExternally; set(value) = definedExternally
    var suffix: String? get() = definedExternally; set(value) = definedExternally
    var zeroPad: Int? get() = definedExternally; set(value) = definedExternally
    var outputArray: Array<AnimationFrameConfig>? get() = definedExternally; set(value) = definedExternally
    var frames: Boolean? get() = definedExternally; set(value) = definedExternally
}

fun GenerateFrameNamesConfig(cb: (GenerateFrameNamesConfig.() -> Unit) = {}) = jsApply(cb)
