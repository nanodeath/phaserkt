@file:JsModule("phaser")
@file:Suppress("unused", "FunctionName")

package phaserkt

external object Math {
    fun FloatBetween(min: Float, max: Float): Float
    fun Between(min: Int, max: Int): Int
}
