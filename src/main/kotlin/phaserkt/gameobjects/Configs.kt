@file:Suppress("FunctionName", "unused")

package phaserkt.gameobjects

import phaserkt.jsApply

/**
 * [Docs](https://photonstorm.github.io/phaser3-docs/global.html#GraphicsOptions).
 */
external interface GraphicsOptions {
    var x: Float? get() = definedExternally; set(value) = definedExternally
    var y: Float? get() = definedExternally; set(value) = definedExternally
}

fun GraphicsOptions(cb: (GraphicsOptions.() -> Unit) = {}) = jsApply(cb)
