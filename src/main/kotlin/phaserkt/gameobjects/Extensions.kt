package phaserkt.gameobjects

import phaserkt.jsLet

fun GameObjectFactory.textWithStyle(x: Float, y: Float, text: dynamic, cb: (dynamic) -> Unit) =
    text(x, y, text, jsLet(cb))


operator fun Container.get(idx: Int) = getAt(idx)