@file:JsModule("phaser")
@file:JsQualifier("GameObjects")
@file:Suppress("unused")

package phaserkt.gameobjects

import phaserkt.Scene
import phaserkt.gameobjects.components.*

external class Container(scene: Scene, x: Float = definedExternally, y: Float = definedExternally, children: Array<GameObject> = definedExternally)
    : GameObject, Alpha, Depth, Transform, Visible {
    override var x: Float get() = definedExternally; set(value) = definedExternally
    override var y: Float get() = definedExternally; set(value) = definedExternally
    override var z: Float get() = definedExternally; set(value) = definedExternally
    override var w: Float get() = definedExternally; set(value) = definedExternally
    override var scaleX: Float get() = definedExternally; set(value) = definedExternally
    override var scaleY: Float get() = definedExternally; set(value) = definedExternally
    override var angle: Float get() = definedExternally; set(value) = definedExternally
    override var rotation: Float get() = definedExternally; set(value) = definedExternally

    override fun setPosition(x: Float, y: Float, z: Float, w: Float): Transform = definedExternally

    override fun setRandomPosition(x: Float, y: Float, width: Float, height: Float): Transform = definedExternally

    override fun getLocalTransformMatrix(tempMatrix: TransformMatrix): TransformMatrix = definedExternally

    override fun getWorldTransformMatrix(tempMatrix: TransformMatrix, parentMatrix: TransformMatrix) = definedExternally

    override var alpha: Float get() = definedExternally; set(value) = definedExternally
    override var visible: Boolean get() = definedExternally; set(value) = definedExternally

    override fun setDepth(value: Int) = definedExternally

    fun getAt(idx: Int): GameObject?

    fun add(child: GameObject): Container
    fun add(children: Array<out GameObject>): Container
    fun addAt(child: GameObject, index: Int = definedExternally): Container
    fun addAt(children: Array<out GameObject>, index: Int = definedExternally): Container

    fun remove(child: GameObject, destroyChild: Boolean = definedExternally): Container
    fun removeAll(destroyChild: Boolean = definedExternally): Container
    fun removeAt(index: Int, destroyChild: Boolean = definedExternally): Container
}