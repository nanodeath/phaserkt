@file:JsModule("phaser")
@file:JsQualifier("GameObjects")
@file:Suppress("unused")

package phaserkt.gameobjects

/**
A Graphics object is a way to draw primitive shapes to your game. Primitives include forms of geometry, such as
Rectangles, Circles, and Polygons. They also include lines, arcs and curves. When you initially create a Graphics
object it will be empty.
 *
 * [Docs](https://photonstorm.github.io/phaser3-docs/Phaser.GameObjects.Graphics.html).
 */
external class Graphics : GameObject {
    /**
    Set the Alpha level of this Game Object. The alpha controls the opacity of the Game Object as it renders.
    Alpha values are provided as a float between 0, fully transparent, and 1, fully opaque.

    If your game is running under WebGL you can optionally specify four different alpha values, each of which
    correspond to the four corners of the Game Object. Under Canvas only the topLeft value given is used.

    Note you must provide either one argument or four arguments -- not two or three.
     */
    fun setAlpha(
        /**
         * The alpha value used for the top-left of the Game Object. If this is the only value given it's applied across the whole Game Object.
         *
         * Default: 1
         */
        topLeft: Float = definedExternally,
        /**
         * The alpha value used for the top-right of the Game Object. WebGL only.
         */
        topRight: Float = definedExternally,
        /**
         * The alpha value used for the bottom-left of the Game Object. WebGL only.
         */
        bottomLeft: Float = definedExternally,
        /**
         * The alpha value used for the bottom-right of the Game Object. WebGL only.
         */
        bottomRight: Float = definedExternally
    ): Graphics

    /**
    The depth of this Game Object within the phaserkt.Scene.

    The depth is also known as the 'z-index' in some environments, and allows you to change the rendering order
    of Game Objects, without actually moving their position in the display list.

    The depth starts from zero (the default value) and increases from that point. A Game Object with a higher depth
    value will always render in front of one with a lower value.

    Setting the depth will queue a depth sort event within the phaserkt.Scene.
     */
    fun setDepth(
        /** The depth of this Game Object. */
        value: Int
    ): Graphics

    fun fillRect(x: Float, y: Float, width: Float, height: Float): Graphics

    fun strokeRect(x: Float, y: Float, width: Float, height: Float): Graphics

    fun fillRoundedRect(x: Float, y: Float, width: Float, height: Float, radius: dynamic = definedExternally): Graphics

    fun strokeRoundedRect(x: Float, y: Float, width: Float, height: Float, radius: dynamic = definedExternally): Graphics

    fun fillStyle(color: Int, alpha: Float = definedExternally): Graphics

    fun lineStyle(lineWidth: Float, color: Int, alpha: Float = definedExternally): Graphics
}
