@file:JsModule("phaser")
@file:JsQualifier("GameObjects")
@file:Suppress("unused")

package phaserkt.gameobjects

import phaserkt.gameobjects.components.Depth
import phaserkt.gameobjects.components.Origin
import phaserkt.gameobjects.components.Transform

external class Zone : GameObject, Transform, Depth, Origin {
    var height: Float = definedExternally
    var width: Float = definedExternally

    // GameObjects.Transform //

    override var x: Float = definedExternally
    override var y: Float = definedExternally
    override var z: Float = definedExternally
    override var w: Float = definedExternally
    override var scaleX: Float = definedExternally
    override var scaleY: Float = definedExternally
    override var angle: Float = definedExternally
    override var rotation: Float = definedExternally
    override fun setPosition(x: Float, y: Float, z: Float, w: Float) = definedExternally
    override fun setRandomPosition(x: Float, y: Float, width: Float, height: Float) = definedExternally
    override fun getLocalTransformMatrix(tempMatrix: dynamic): dynamic = definedExternally
    override fun getWorldTransformMatrix(tempMatrix: dynamic, parentMatrix: dynamic): dynamic = definedExternally

    // Depth
    override var depth: Int? get() = definedExternally; set(value) = definedExternally
    override fun setDepth(value: Int) = definedExternally

    // Origin

    override fun setOrigin(x: Float, y: Float): Origin = definedExternally
}