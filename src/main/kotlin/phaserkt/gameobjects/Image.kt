@file:JsModule("phaser")
@file:JsQualifier("GameObjects")
@file:Suppress("unused")

package phaserkt.gameobjects

import phaserkt.gameobjects.components.*

abstract external class Image : GameObject, Alpha, Depth, Origin, Size, Tint, Transform, Visible {
    fun setFrame(frame: dynamic, updateSize: Boolean = definedExternally, updateOrigin: Boolean = definedExternally): Image
    fun setTexture(key: String, frame: dynamic = definedExternally): Image
}