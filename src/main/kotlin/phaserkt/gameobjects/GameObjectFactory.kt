@file:JsModule("phaser")
@file:JsQualifier("GameObjects")
@file:Suppress("unused")

package phaserkt.gameobjects

import phaserkt.types.gameobjects.text.TextStyle

/**
 * [Docs](https://photonstorm.github.io/phaser3-docs/Phaser.GameObjects.GameObjectFactory.html)
 */
external class GameObjectFactory {
    fun image(x: Float, y: Float, texture: String, frame: dynamic = definedExternally): Image
    fun text(x: Float, y: Float, text: dynamic, style: TextStyle = definedExternally): Text
    fun tileSprite(
            x: Float,
            y: Float,
            width: Float,
            height: Float,
            texture: String,
            frame: dynamic = definedExternally
    ): TileSprite

    /** Creates a new Graphics Game Object and adds it to the Scene. */
    fun graphics(graphicsOptions: GraphicsOptions = definedExternally): Graphics

    /** Creates a new Group Game Object and adds it to the Scene. */
    fun group(children: dynamic = definedExternally, config: dynamic = definedExternally): Group

    /** Creates a new Sprite Game Object and adds it to the Scene. */
    // https://github.com/photonstorm/phaser/blob/d82b46c4d2b6a2879568038417999215475cf09c/src/gameobjects/sprite/SpriteFactory.js#L25
    fun sprite(x: Float, y: Float, texture: String, frame: dynamic = definedExternally): Sprite

    /** Creates a new Container Game Object and adds it to the Scene. */
    fun container(x: Float, y: Float, children: Array<GameObject> = definedExternally): Container

    /** Creates a new Zone Game Object and adds it to the Scene. */
    fun zone(x: Number, y: Number, width: Number, height: Number): Zone

    /**
     * Adds an existing Game Object to this Scene.
     * If the Game Object renders, it will be added to the Display List. If it has a `preUpdate` method, it will be
     * added to the Update List.
     */
    fun existing(child: GameObject): GameObject
}
