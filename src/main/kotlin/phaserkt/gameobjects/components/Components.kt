@file:JsModule("phaser")
@file:JsQualifier("GameObjects.Components")

package phaserkt.gameobjects.components

import phaserkt.gameobjects.GameObject
import phaserkt.textures.Frame

external interface Tint {
    fun setTint(
        topLeft: Int = definedExternally,
        topRight: Int = definedExternally,
        bottomLeft: Int = definedExternally,
        bottomRight: Int = definedExternally
    ): Tint
}

external interface Transform {
    var x: Float
    var y: Float
    var z: Float
    var w: Float
    var scaleX: Float
    var scaleY: Float
    var angle: Float
    var rotation: Float
    fun setPosition(
        x: Float = definedExternally,
        y: Float = definedExternally,
        z: Float = definedExternally,
        w: Float = definedExternally
    ): Transform

    fun setRandomPosition(
        x: Float = definedExternally,
        y: Float = definedExternally,
        width: Float = definedExternally,
        height: Float = definedExternally
    ): Transform

    fun getLocalTransformMatrix(tempMatrix: TransformMatrix = definedExternally): TransformMatrix
    fun getWorldTransformMatrix(
        tempMatrix: TransformMatrix = definedExternally,
        parentMatrix: TransformMatrix = definedExternally
    ): TransformMatrix
}

external interface Gravity {
    fun setGravity(x: Float, y: Float = definedExternally): Gravity
    fun setGravityX(x: Float): Gravity
    fun setGravityY(y: Float): Gravity
}

external interface ScrollFactor {
    var scrollFactorX: Float
    var scrollFactorY: Float
    fun setScrollFactor(x: Float, y: Float = definedExternally): ScrollFactor
    /** The depth of this Game Object within the phaserkt.Scene. */
    fun setDepth(
        /** The depth of this Game Object. */
        value: Int
    ): ScrollFactor
}

external interface Size {
    val width: Float
    val height: Float
    var displayWidth: Float
    var displayHeight: Float
    fun setSizeToFrame(frame: Frame = definedExternally): Size
    fun setSize(width: Float, height: Float): Size
    fun setDisplaySize(width: Float, height: Float): Size
}

external interface Origin {
    fun setOrigin(x: Float, y: Float = definedExternally): Origin
}

external interface TextureCrop {
    fun setTexture(key: String, frame: dynamic): TextureCrop
}

external interface Animation {
    fun play(key: String, ignoreIfPlaying: Boolean = definedExternally, startFrame: Int = definedExternally): GameObject
    fun stop()
}

/**
 * [Docs](https://github.com/photonstorm/phaser/blob/v3.12.0/src/gameobjects/components/TransformMatrix.js#L10).
 */
external interface TransformMatrix {
    var a: Float
    var b: Float
    var c: Float
    var d: Float
    var decomposedMatrix: dynamic
    var e: Float
    var f: Float
    val rotation: Float
    val scaleX: Float
    val scaleY: Float
    var tx: Float
    var ty: Float
}

external interface Alpha {
    /** Alpha, in the range [0,1]. */
    // https://github.com/photonstorm/phaser/blob/c91ed91ce37ae0838ee36adc56c63e81d48ebe66/src/gameobjects/components/Alpha.js#L138
    var alpha: Float
}

external interface Visible {
    // https://github.com/photonstorm/phaser/blob/c91ed91ce37ae0838ee36adc56c63e81d48ebe66/src/gameobjects/components/Visible.js#L40
    var visible: Boolean
}

external interface Depth {
    var depth: Int? get() = definedExternally; set(value) = definedExternally
    fun setDepth(value: Int)
}