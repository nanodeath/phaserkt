@file:JsModule("phaser")
@file:JsQualifier("GameObjects")
@file:Suppress("unused")

package phaserkt.gameobjects

import phaserkt.Scene
import phaserkt.events.EventEmitter
import phaserkt.gameobjects.components.*

open external class GameObject(scene: Scene, type: String) : EventEmitter {
    var name: String

    fun setInteractive(shape: dynamic = definedExternally, callback: dynamic = definedExternally, dropZone: Boolean = definedExternally): GameObject
}

open external class Sprite(scene: Scene, x: Number, y: Number, texture: String, frame: dynamic = definedExternally) : GameObject, Tint, Alpha, Visible, Transform, Depth {
    fun setScale(x: Float, y: Float = definedExternally): Sprite
    val anims: Animation

    override fun setTint(topLeft: Int, topRight: Int, bottomLeft: Int, bottomRight: Int): Tint = definedExternally
    override var alpha: Float = definedExternally
    override var visible: Boolean = definedExternally

    // Transform
    override var x: Float = definedExternally
    override var y: Float = definedExternally
    override var z: Float = definedExternally
    override var w: Float = definedExternally
    override var scaleX: Float = definedExternally
    override var scaleY: Float = definedExternally
    override var angle: Float = definedExternally
    override var rotation: Float = definedExternally
    override fun setPosition(x: Float, y: Float, z: Float, w: Float) = definedExternally
    override fun setRandomPosition(x: Float, y: Float, width: Float, height: Float) = definedExternally
    override fun getLocalTransformMatrix(tempMatrix: dynamic) = definedExternally
    override fun getWorldTransformMatrix(tempMatrix: dynamic, parentMatrix: dynamic) = definedExternally

    // Depth
    override var depth: Int? get() = definedExternally; set(value) = definedExternally
    override fun setDepth(value: Int) = definedExternally
}

external class Text : GameObject, ScrollFactor, Alpha, Visible, Origin, Transform {
    val text: String
    fun setText(value: dynamic): Text

    override var scrollFactorX: Float get() = definedExternally; set(value) = definedExternally
    override var scrollFactorY: Float get() = definedExternally; set(value) = definedExternally
    override fun setScrollFactor(x: Float, y: Float): Text = definedExternally
    override fun setDepth(value: Int): Text = definedExternally
    override var alpha: Float = definedExternally
    override var visible: Boolean = definedExternally
    override fun setOrigin(x: Float, y: Float): Text = definedExternally

    // Transform
    override var x: Float = definedExternally
    override var y: Float = definedExternally
    override var z: Float = definedExternally
    override var w: Float = definedExternally
    override var scaleX: Float = definedExternally
    override var scaleY: Float = definedExternally
    override var angle: Float = definedExternally
    override var rotation: Float = definedExternally
    override fun setPosition(x: Float, y: Float, z: Float, w: Float) = definedExternally
    override fun setRandomPosition(x: Float, y: Float, width: Float, height: Float) = definedExternally
    override fun getLocalTransformMatrix(tempMatrix: dynamic) = definedExternally
    override fun getWorldTransformMatrix(tempMatrix: dynamic, parentMatrix: dynamic) = definedExternally

    fun setPadding(left: dynamic = definedExternally, top: Float? = definedExternally, right: Float? = definedExternally, bottom: Float? = definedExternally): Text

    fun setShadow(x: Float = definedExternally, y: Float = definedExternally, color: String = definedExternally, blur: Float = definedExternally, shadowStroke: Boolean = definedExternally, shadowFill: Boolean = definedExternally): Text
    fun setStroke(color: String, thickness: Float) : Text
}

external class TileSprite : GameObject, Tint, Transform,
        ScrollFactor {
    var tilePositionX: Float
    var tilePositionY: Float
    fun setTilePosition(x: Float = definedExternally, y: Float = definedExternally): TileSprite

    override fun setTint(topLeft: Int, topRight: Int, bottomLeft: Int, bottomRight: Int) = definedExternally

    // Transform
    override var x: Float = definedExternally
    override var y: Float = definedExternally
    override var z: Float = definedExternally
    override var w: Float = definedExternally
    override var scaleX: Float = definedExternally
    override var scaleY: Float = definedExternally
    override var angle: Float = definedExternally
    override var rotation: Float = definedExternally
    override fun setPosition(x: Float, y: Float, z: Float, w: Float) = definedExternally
    override fun setRandomPosition(x: Float, y: Float, width: Float, height: Float) = definedExternally
    override fun getLocalTransformMatrix(tempMatrix: dynamic) = definedExternally
    override fun getWorldTransformMatrix(tempMatrix: dynamic, parentMatrix: dynamic) = definedExternally

    override var scrollFactorX: Float = definedExternally
    override var scrollFactorY: Float = definedExternally
    override fun setScrollFactor(x: Float, y: Float) = definedExternally
    override fun setDepth(value: Int): Text = definedExternally
}
