@file:JsModule("phaser")
@file:JsQualifier("GameObjects")

package phaserkt.gameobjects

external class Group {
    fun <T> create(x: Number = definedExternally, y: Number = definedExternally, key: String = definedExternally,
                   frame: dynamic = definedExternally, visible: Boolean = definedExternally, active: Boolean = definedExternally)
            : T

    // https://github.com/photonstorm/phaser/blob/c91ed91ce37ae0838ee36adc56c63e81d48ebe66/src/gameobjects/group/Group.js#L469
    fun add(child: GameObject, addToScene: Boolean = definedExternally): Group

    fun getChildren(): Array<GameObject>
}