@file:JsModule("phaser")
@file:JsQualifier("GameObjects")
@file:Suppress("unused")

package phaserkt.gameobjects

import phaserkt.tilemaps.Tilemap
import phaserkt.tilemaps.TilemapConfig

/**
The Game Object Creator is a phaserkt.Scene plugin that allows you to quickly create many common
types of Game Objects and return them. Unlike the Game Object Factory, they are not automatically
added to the phaserkt.Scene.

Game Objects directly register themselves with the Creator and inject their own creation
methods into the class.
 */
external class GameObjectCreator {
    fun tilemap(config: TilemapConfig? = definedExternally): Tilemap
}
