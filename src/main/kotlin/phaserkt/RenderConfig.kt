@file:Suppress("unused", "FunctionName")

package phaserkt

external interface RenderConfig {
    /**
     * If true, sets [antialias] to false and [roundPixels] to true.
     *
     * See [round pixels changes](https://github.com/photonstorm/phaser/blob/f869794346cc8770589041447cc8a8973f09e289/CHANGELOG.md#round-pixels-changes).
     *
     * Default: false.
     */
    var pixelArt: Boolean? get() = definedExternally; set(value) = definedExternally

    /**
     * Whether to antialias the output.
     *
     * See [round pixels changes](https://github.com/photonstorm/phaser/blob/f869794346cc8770589041447cc8a8973f09e289/CHANGELOG.md#round-pixels-changes).
     *
     * Default: true.
     */
    var antialias: Boolean? get() = definedExternally; set(value) = definedExternally

    /**
     * Whether to floor fractional pixel values.
     *
     * See [round pixels changes](https://github.com/photonstorm/phaser/blob/f869794346cc8770589041447cc8a8973f09e289/CHANGELOG.md#round-pixels-changes).
     *
     * Default: false.
     */
    var roundPixels: Boolean? get() = definedExternally; set(value) = definedExternally
}

fun RenderConfig(cb: (RenderConfig.() -> Unit) = {}) = jsApply(cb)
