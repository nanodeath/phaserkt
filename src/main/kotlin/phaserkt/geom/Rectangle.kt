@file:JsModule("phaser")
@file:JsQualifier("Geom")
@file:Suppress("unused")

package phaserkt.geom

external class Rectangle(
        x: Float = definedExternally,
        y: Float = definedExternally,
        width: Float = definedExternally,
        height: Float = definedExternally
)