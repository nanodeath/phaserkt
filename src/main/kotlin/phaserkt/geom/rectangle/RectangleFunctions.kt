@file:JsModule("phaser")
@file:JsQualifier("Geom.Rectangle")
@file:Suppress("unused")

package phaserkt.geom.rectangle

import phaserkt.geom.Rectangle

external fun Contains(rect: Rectangle, x: Float, y: Float): Boolean
