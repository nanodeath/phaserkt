@file:Suppress("FunctionName", "unused")

package phaserkt

external interface GameConfig {
    var type: Type? get() = definedExternally; set(value) = definedExternally
    var width: Int? get() = definedExternally; set(value) = definedExternally
    var height: Int? get() = definedExternally; set(value) = definedExternally
    var zoom: Float? get() = definedExternally; set(value) = definedExternally
    var render: RenderConfig? get() = definedExternally; set(value) = definedExternally

    // https://github.com/photonstorm/phaser/blob/aeda1c042bbdf70b338bb84e2d34a4278272b215/src/scene/SceneManager.js#L186
    // Either a Phaser.phaserkt.Scene, an object (passed to phaserkt.Scene constructor), or a function that return a phaserkt.Scene...or something else.
    var scene: dynamic get() = definedExternally; set(value) = definedExternally
    var physics: dynamic get() = definedExternally; set(value) = definedExternally

//        var physics: Any? get() = definedExternally; set(value) = definedExternally
//        var resolution: Number? get() = definedExternally; set(value) = definedExternally
    /**
     * The parent in which to add the element. Can be a string which is passed to `getElementById` or an actual DOM object.
     *
     * Defaults to document.body.
     */
    var parent: dynamic get() = definedExternally; set(value) = definedExternally
//        var canvas: HTMLCanvasElement? get() = definedExternally; set(value) = definedExternally
//        var canvasStyle: String? get() = definedExternally; set(value) = definedExternally
//        var context: CanvasRenderingContext2D? get() = definedExternally; set(value) = definedExternally
//        var seed: Array<String>? get() = definedExternally; set(value) = definedExternally
//        var title: String? get() = definedExternally; set(value) = definedExternally
//        var url: String? get() = definedExternally; set(value) = definedExternally
//        var version: String? get() = definedExternally; set(value) = definedExternally
//        var autoFocus: Boolean? get() = definedExternally; set(value) = definedExternally
//        var input: dynamic /* Boolean | InputConfig */ get() = definedExternally; set(value) = definedExternally
//        var disableContextMenu: Boolean? get() = definedExternally; set(value) = definedExternally
//        var banner: dynamic /* Boolean | BannerConfig */ get() = definedExternally; set(value) = definedExternally
//        var dom: DOMContainerConfig? get() = definedExternally; set(value) = definedExternally
//        var fps: FPSConfig? get() = definedExternally; set(value) = definedExternally
    /** The background color of the game canvas. The default is black. */
        var backgroundColor: dynamic /* String | Number */ get() = definedExternally; set(value) = definedExternally
//        var callbacks: CallbacksConfig? get() = definedExternally; set(value) = definedExternally
//        var loader: LoaderConfig? get() = definedExternally; set(value) = definedExternally
//        var images: ImagesConfig? get() = definedExternally; set(value) = definedExternally
//        var plugins: dynamic /* Array<PluginObjectItem> | PluginObject */ get() = definedExternally; set(value) = definedExternally
    var scale: ScaleConfig? get() = definedExternally; set(value) = definedExternally
}

fun GameConfig(cb: (GameConfig.() -> Unit) = {}) = jsApply(cb)

@JsModule("phaser")
external enum class Type {
    CANVAS, WEBGL, AUTO
}
