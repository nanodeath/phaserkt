@file:JsModule("phaser")
@file:JsQualifier("Tweens")

package phaserkt.tweens

import phaserkt.tweens.tween.Tween

external class TweenManager {
    fun add(config: TweenBuilderConfig): Tween
}