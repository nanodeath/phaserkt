package phaserkt.tweens

import phaserkt.jsApply
import phaserkt.tweens.tween.Tween

typealias TweenOnCompleteCallback = (Tween, dynamic, dynamic) -> Unit

external interface TweenBuilderConfig {
    var targets: dynamic get() = definedExternally; set(value) = definedExternally
    var duration: Int? get() = definedExternally; set(value) = definedExternally
    var onComplete: TweenOnCompleteCallback? get() = definedExternally; set(value) = definedExternally

    var repeat: Int? get() = definedExternally; set(value) = definedExternally
    var yoyo: Boolean? get() = definedExternally; set(value) = definedExternally
}

fun TweenBuilderConfig(cb: (TweenBuilderConfig.() -> Unit) = {}) = jsApply(cb)