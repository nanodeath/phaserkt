@file:JsModule("phaser")
@file:JsQualifier("Tilemaps")
@file:Suppress("unused")

package phaserkt.tilemaps

import phaserkt.gameobjects.GameObject

external class Tilemap {
    /**
    Adds an image to the map to be used as a tileset. A single map may use multiple tilesets.
    Note that the tileset name can be found in the JSON file exported from Tiled, or in the Tiled
    editor.
     * @return the Tileset object that was created or updated, or null if it failed.
     */
    fun addTilesetImage(
        /** The name of the tileset as specified in the map data. */
        tilesetName: String,
        /**
        The key of the Phaser.Cache image used for this tileset. If
        undefined or null it will look for an image with a key matching the tilesetName parameter.
         */
        key: String? = definedExternally,
        /**
        The width of the tile (in pixels) in the Tileset Image. If not
        given it will default to the map's tileWidth value, or the tileWidth specified in the Tiled
        JSON file.
         */
        tileWidth: Int? = definedExternally,
        /**
        The height of the tiles (in pixels) in the Tileset Image. If
        not given it will default to the map's tileHeight value, or the tileHeight specified in the
        Tiled JSON file.
         */
        tileHeight: Int? = definedExternally,
        /**
        The margin around the tiles in the sheet (in pixels). If not
        specified, it will default to 0 or the value specified in the Tiled JSON file.
         */
        tileMargin: Int? = definedExternally,
        /**
        The spacing between each the tile in the sheet (in pixels).
        If not specified, it will default to 0 or the value specified in the Tiled JSON file.
         */
        tileSpacing: Int? = definedExternally,
        /**
        If adding multiple tilesets to a blank map, specify the starting
        GID this set will use here. Default: 0.
         */
        gid: Int? = definedExternally
    ): Tileset?

    /**
    Creates a new [StaticTilemapLayer] that renders the LayerData associated with the given
    [layerID]. The currently selected layer in the map is set to this new layer.

    The layerID is important. If you've created your map in Tiled then you can get this by
    looking in Tiled and looking at the layer name. Or you can open the JSON file it exports and
    look at the layers[].name value. Either way it must match.

    It's important to remember that a static layer cannot be modified. See [StaticTilemapLayer] for
    more information.
     */
    fun createStaticLayer(
        /**
         * The layer array index value, or if a string is given, the layer name from Tiled.
         * phaserkt.Type: [Int] or [String].
         */
        layerID: dynamic,
        /** The tileset the new layer will use. */
        tileset: Tileset,
        /**
         * The x position to place the layer in the world. If not specified, it will default to the layer offset from
         * Tiled or 0.
         */
        x: Float,
        /**
         * The y position to place the layer in the world. If not specified, it will default to the layer offset from
         * Tiled or 0.
         */
        y: Float
    ): StaticTilemapLayer

    /**
     * The width of the map in pixels based on width × tileWidth.
     */
    val widthInPixels: Int

    /**
     * The height of the map in pixels based on height × tileHeight.
     */
    val heightInPixels: Int

    /**
    Find the first object in the given object layer that satisfies the provided testing function.
    I.e. finds the first object for which callback returns true. Similar to
    Array.prototype.find in vanilla JS.
     */
    fun findObject(
        /** [ObjectLayer] or string. The name of an object layer (from Tiled) or an ObjectLayer instance. */
        objectLayer: dynamic,
        /** The callback. Each object in the given area will be passed to this callback as the first and only parameter. */
        callback: (value: GameObject, index: Int, array: Array<GameObject>) -> Boolean,
        /** The context under which the callback should be run. */
        context: dynamic = definedExternally
    ): GameObject?
}
