@file:JsModule("phaser")
@file:JsQualifier("Tilemaps")

package phaserkt.tilemaps

/**
A Tileset is a combination of an image containing the tiles and a container for data about
each tile.
 */
external class Tileset
