@file:JsModule("phaser")
@file:JsQualifier("Tilemaps")
@file:Suppress("unused")

package phaserkt.tilemaps

import phaserkt.gameobjects.Graphics

/**
 * A StaticTilemapLayer is a game object that renders LayerData from a Tilemap. A
StaticTilemapLayer can only render tiles from a single tileset.

A StaticTilemapLayer is optimized for speed over flexibility. You cannot apply per-tile
effects like tint or alpha. You cannot change the tiles in a StaticTilemapLayer. Use this
over a DynamicTilemapLayer when you don't need either of those features.

[StaticTilemapLayer](https://photonstorm.github.io/phaser3-docs/Phaser.phaserkt.StaticTilemapLayer.html)
 */
external class StaticTilemapLayer {
    /**
    Sets collision on the tiles within a layer by checking tile properties. If a tile has a property
    that matches the given properties object, its collision flag will be set. The collides
    parameter controls if collision will be enabled (true) or disabled (false). Passing in
    { collides: true } would update the collision flag on any tiles with a "collides" property that
    has a value of true. Any tile that doesn't have "collides" set to true will be ignored. You can
    also use an array of values, e.g. { types: ["stone", "lava", "sand" ] }. If a tile has a
    "types" property that matches any of those values, its collision flag will be updated.

    [Docs](https://photonstorm.github.io/phaser3-docs/Phaser.phaserkt.StaticTilemapLayer.html#setCollisionByProperty__anchor)
     */
    fun setCollisionByProperty(
        /** An object with tile properties and corresponding values that should be checked. */
        properties: dynamic,
        /** If true it will enable collision. If false it will clear collision. Default: true. */
        collides: Boolean = definedExternally,
        /** Whether or not to recalculate the tile faces after the update. Default: true. */
        recalculateFaces: Boolean = definedExternally
    ): StaticTilemapLayer

    /**
    Draws a debug representation of the layer to the given Graphics. This is helpful when you want to
    get a quick idea of which of your tiles are colliding and which have interesting faces. The tiles
    are drawn starting at (0, 0) in the Graphics, allowing you to place the debug representation
    wherever you want on the screen.

    [Docs](https://photonstorm.github.io/phaser3-docs/Phaser.phaserkt.StaticTilemapLayer.html#renderDebug__anchor)
     */
    fun renderDebug(
        /** The target Graphics object to draw upon. */
        graphics: Graphics,
        /** An object specifying the colors to use for the debug drawing. */
        styleConfig: StaticTilemapLayerDebugStyleConfig = definedExternally
    ): StaticTilemapLayer

    /**
    The depth of this Game Object within the phaserkt.Scene.

    [Docs](https://photonstorm.github.io/phaser3-docs/Phaser.phaserkt.StaticTilemapLayer.html#setDepth__anchor)
     */
    fun setDepth(
        /** The depth of this Game Object. */
        depth: Int
    ): StaticTilemapLayer
}
