@file:Suppress("unused", "FunctionName")

package phaserkt.tilemaps

import phaserkt.display.Color
import phaserkt.jsApply

external interface StaticTilemapLayerDebugStyleConfig {
    /**
     * Color to use for drawing a filled rectangle at non-colliding tile locations. If set to null, non-colliding tiles
     * will not be drawn.
     */
    var tileColor: Color? get() = definedExternally; set(value) = definedExternally
    /**
     * Color to use for drawing a filled rectangle at colliding tile locations. If set to null, colliding tiles will not
     * be drawn.
     */
    var collidingTileColor: Color? get() = definedExternally; set(value) = definedExternally
    /**
     * Color to use for drawing a line at interesting tile faces. If set to null, interesting tile faces will not be
     * drawn.
     */
    var faceColor: Color? get() = definedExternally; set(value) = definedExternally
}

fun StaticTilemapLayerDebugStyleConfig(cb: (StaticTilemapLayerDebugStyleConfig.() -> Unit) = {}) =
    jsApply(cb)

external interface TilemapConfig {
    /** The key in the Phaser cache that corresponds to the loaded tilemap data. */
    var key: String? get() = definedExternally; set(value) = definedExternally
    /** Instead of loading from the cache, you can also load directly from a 2D array of tile indexes. */
    var data: Array<IntArray>? get() = definedExternally; set(value) = definedExternally
    /** The width of a tile in pixels. Default: 32. */
    var tileWidth: Int? get() = definedExternally; set(value) = definedExternally
    /** The height of a tile in pixels. Default: 32. */
    var tileHeight: Int? get() = definedExternally; set(value) = definedExternally
    /** The width of the map in tiles. Default: 10. */
    var width: Int? get() = definedExternally; set(value) = definedExternally
    /** The height of the map in tiles. Default: 10. */
    var height: Int? get() = definedExternally; set(value) = definedExternally

    /**
    Controls how empty tiles, tiles with an index of -1,
    in the map data are handled. If `true`, empty locations will get a value of `null`. If `false`,
    empty location will get a Tile object with an index of -1. If you've a large sparsely populated
    map and the tile data doesn't need to change then setting this value to `true` will help with
    memory consumption. However if your map is small or you need to update the tiles dynamically,
    then leave the default value set. Default: false.
     */
    var insertNull: Boolean? get() = definedExternally; set(value) = definedExternally
}

fun TilemapConfig(cb: (TilemapConfig.() -> Unit) = {}) = jsApply(cb)
