@file:Suppress("unused", "MemberVisibilityCanBePrivate", "CanBeParameter", "FunctionName")

package phaserkt

import phaserkt.scale.CenterModeType
import phaserkt.scale.ScaleModeType

external interface ScaleConfig {
    /** Set using [setWidth] instead. */
    var width: dynamic get() = definedExternally; set(value) = definedExternally
    /** Set using [setHeight] instead. */
    var height: dynamic get() = definedExternally; set(value) = definedExternally
    var mode: ScaleModeType? get() = definedExternally; set(value) = definedExternally
    var parent: String? get() = definedExternally; set(value) = definedExternally
    var autoCenter: CenterModeType? get() = definedExternally; set(value) = definedExternally
}

fun ScaleConfig.setWidth(scaleSize: ScaleSize) {
    width = scaleSize.value
}

fun ScaleConfig.setHeight(scaleSize: ScaleSize) {
    height = scaleSize.value
}

sealed class ScaleSize(val value: dynamic) {
    class Pixels(val pixelCount: Int) : ScaleSize(pixelCount)
    object FullSize : ScaleSize("100%")
}

fun ScaleConfig(cb: (ScaleConfig.() -> Unit) = {}): ScaleConfig = jsApply(cb)