@file:JsModule("phaser")
@file:JsQualifier("Scenes")
@file:Suppress("unused")

package phaserkt.scene

import phaserkt.Scene
import phaserkt.types.scenes.SettingsConfig

external class ScenePlugin {
    val key: String
    val manager: SceneManager
    val scene: Scene
    val settings: dynamic
    val systems: dynamic
    val transitionProgress: Float

    fun add(key: String, sceneConfig: Scene, autoStart: Boolean, data: dynamic = definedExternally): Scene
    fun add(key: String, sceneConfig: SettingsConfig, autoStart: Boolean, data: dynamic = definedExternally): Scene
    fun add(key: String, sceneConfig: dynamic, autoStart: Boolean, data: dynamic = definedExternally): Scene

    fun bringToTop(key: String = definedExternally): ScenePlugin

    fun get(key: String): Scene?

    fun getIndex(): Int
    fun getIndex(key: String): Int
    fun getIndex(key: Scene): Int

    fun isActive(): Boolean
    fun isActive(key: String): Boolean

    fun isPaused(): Boolean
    fun isPaused(key: String): Boolean

    fun isSleeping(): Boolean
    fun isSleeping(key: String): Boolean

    fun isVisible(): Boolean
    fun isVisible(key: String): Boolean

    fun launch(key: String, data: dynamic = definedExternally): ScenePlugin

    /** Moves the current scene to be above scene with key [key]. */
    fun moveAbove(key: String): ScenePlugin

    /** Moves scene [keyB] to be above scene [keyA]. */
    fun moveAbove(keyA: String, keyB: String): ScenePlugin

    /** Moves the current scene to be below scene with key [key]. */
    fun moveBelow(key: String): ScenePlugin

    /** Moves scene [keyB] to be below scene [keyA]. */
    fun moveBelow(keyA: String, keyB: String): ScenePlugin

    /** Moves the current scene down one position. */
    fun moveDown(): ScenePlugin

    /** Moves scene [key] down one position. */
    fun moveDown(key: String): ScenePlugin

    /** Moves the current scene up one position. */
    fun moveUp(): ScenePlugin

    /** Moves scene [key] up one position. */
    fun moveUp(key: String): ScenePlugin

    fun pause(key: String = definedExternally, data: dynamic = definedExternally): ScenePlugin

    fun remove(key: String = definedExternally): ScenePlugin

    fun restart(data: dynamic = definedExternally): ScenePlugin

    fun resume(key: String = definedExternally, data: dynamic = definedExternally): ScenePlugin

    fun run(key: String, data: dynamic = definedExternally): ScenePlugin

    fun sendToBack(key: String = definedExternally): ScenePlugin

    fun setActive(value: Boolean, key: String = definedExternally, data: dynamic = definedExternally): ScenePlugin

    fun setVisible(value: Boolean, key: String = definedExternally): ScenePlugin

    fun sleep(key: String = definedExternally, data: dynamic = definedExternally): ScenePlugin

    fun start(key: String = definedExternally, data: dynamic = definedExternally): ScenePlugin

    fun stop(key: String = definedExternally): ScenePlugin

    /** Swaps the current scene with scene [key]. */
    fun swapPosition(key: String): ScenePlugin

    /** Swaps scene [keyB] with scene [keyA]. */
    fun swapPosition(keyA: String, keyB: String): ScenePlugin

    fun switch(key: String): ScenePlugin

    // Pending config object
//    fun transition(config: dynamic): Boolean

    fun wake(key: String = definedExternally, data: dynamic = definedExternally): ScenePlugin
}