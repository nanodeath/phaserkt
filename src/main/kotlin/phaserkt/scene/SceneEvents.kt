@file:JsModule("phaser")
@file:JsQualifier("Scenes")
@file:Suppress("unused")

package phaserkt.scene

@JsName("Events")
external enum class SceneEvents {
    BOOT, CREATE, DESTROY, PAUSE, POST_UPDATE, PRE_UPDATE, READY, RENDER, RESUME, SHUTDOWN, SLEEP, START,
    TRANSITION_COMPLETE, TRANSITION_INIT, TRANSITION_OUT, TRANSITION_START, TRANSITION_WAKE, UPDATE, WAKE
}