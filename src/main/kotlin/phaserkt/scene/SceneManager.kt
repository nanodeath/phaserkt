@file:JsModule("phaser")
@file:JsQualifier("Scenes")
@file:Suppress("unused")

package phaserkt.scene

import phaserkt.Scene
import phaserkt.types.scenes.SettingsConfig

external class SceneManager {
    /**
     * Adds a new Scene into the SceneManager. You must give each Scene a unique key by which you'll identify it.
     * @param key A unique key used to reference the Scene, i.e. `MainMenu` or `Level1`.
     * @param sceneConfig The [Scene] instance to add.
     * @param autoStart If `true` the Scene will be started immediately after being added. Defaults to `false`.
     * @param data Optional data object. This will be set as Scene.settings.data and passed to `Scene.init`.
     * @return The added Scene, if it was added immediately, otherwise `null`.
     */
    fun add(key: String, sceneConfig: Scene, autoStart: Boolean = definedExternally, data: dynamic = definedExternally): Scene?
    /**
     * Adds a new Scene into the SceneManager. You must give each Scene a unique key by which you'll identify it.
     * @param key A unique key used to reference the Scene, i.e. `MainMenu` or `Level1`.
     * @param sceneConfig The [SettingsConfig] used to construct a new [Scene].
     * @param autoStart If `true` the Scene will be started immediately after being added. Defaults to `false`.
     * @param data Optional data object. This will be set as Scene.settings.data and passed to `Scene.init`.
     * @return The added Scene, if it was added immediately, otherwise `null`.
     */
    fun add(key: String, sceneConfig: SettingsConfig, autoStart: Boolean = definedExternally, data: dynamic = definedExternally): Scene?
    /**
     * Adds a new Scene into the SceneManager. You must give each Scene a unique key by which you'll identify it.
     * @param key A unique key used to reference the Scene, i.e. `MainMenu` or `Level1`.
     * @param sceneConfig A POJO, a class that extends [Scene], or a JS function.
     * @param autoStart If `true` the Scene will be started immediately after being added. Defaults to `false`.
     * @param data Optional data object. This will be set as Scene.settings.data and passed to `Scene.init`.
     * @return The added Scene, if it was added immediately, otherwise `null`.
     */
    fun add(key: String, sceneConfig: dynamic, autoStart: Boolean = definedExternally, data: dynamic = definedExternally): Scene?
    fun start(key: String, data: dynamic = definedExternally): SceneManager
    fun pause(key: String, data: dynamic = definedExternally): SceneManager
    fun remove(key: String): SceneManager
    fun remove(key: Scene): SceneManager
    fun stop(key: String): SceneManager

    val keys: dynamic
    val scenes: Array<Scene>
}