@file:JsModule("phaser")
@file:JsQualifier("Core")
@file:Suppress("unused")

package phaserkt.events

@JsName("Events")
external enum class CoreEvents {
    BLUR, BOOT, CONTEXT_LOST, CONTEXT_RESTORED, DESTROY, FOCUS, HIDDEN, PAUSE, POST_RENDER, POST_STEP, PRE_RENDER, PRE_STEP,
    READY, RESUME, STEP, VISIBLE
}
