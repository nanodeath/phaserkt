@file:JsModule("phaser")
@file:JsQualifier("Events")

package phaserkt.events

open external class EventEmitter {
    fun on(event: dynamic, fn: dynamic, context: dynamic = definedExternally): EventEmitter
    fun once(event: dynamic, fn: dynamic, context: dynamic = definedExternally): EventEmitter
    /**
     * Calls each of the listeners registered for a given event.
     * @return true if the event had listeners, else false.
     */
    fun emit(
            /** The event name. */
            event: dynamic,
            /** Additional arguments that will be passed to the event handler. */
            args: dynamic = definedExternally): Boolean
}