@file:Suppress("FunctionName", "unused")

package phaserkt.camera.controls

import phaserkt.cameras.scene2d.Camera
import phaserkt.input.keyboard.Key
import phaserkt.jsApply

external interface FixedKeyControlConfig {
    var camera: Camera? get() = definedExternally; set(value) = definedExternally
    var left: Key? get() = definedExternally; set(value) = definedExternally
    var right: Key? get() = definedExternally; set(value) = definedExternally
    var up: Key? get() = definedExternally; set(value) = definedExternally
    var down: Key? get() = definedExternally; set(value) = definedExternally
    var zoomIn: Key? get() = definedExternally; set(value) = definedExternally
    var zoomOut: Key? get() = definedExternally; set(value) = definedExternally
    var zoomSpeed: Float? get() = definedExternally; set(value) = definedExternally
    var speed: dynamic get() = definedExternally; set(value) = definedExternally
}

fun FixedKeyControlConfig(cb: (FixedKeyControlConfig.() -> Unit) = {}) = jsApply(cb)
