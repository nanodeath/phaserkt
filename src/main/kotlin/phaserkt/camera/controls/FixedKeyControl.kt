@file:JsModule("phaser")
@file:JsQualifier("Cameras.Controls")
@file:Suppress("unused")

package phaserkt.camera.controls

external class FixedKeyControl(fixedKeyControlConfig: FixedKeyControlConfig) {
    /**
     * Apply key states (e.g. up, down, zoomIn) to the camera.
     *
     * Movement speed is scaled by [delta]. Zoom speed is...not.
     *
     * Zoom is enforced to be > 0.1.
     */
    fun update(
        /** Seconds since last update. Default 1. */
        delta: Float = definedExternally
    )
}

