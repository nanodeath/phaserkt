@file:JsModule("phaser")
@file:JsQualifier("Math")
@file:Suppress("unused")

package phaserkt.math

/**
 * [Docs](https://photonstorm.github.io/phaser3-docs/phaserkt.Vector2h.Vector2.html).
 */
external class Vector2 {
    var x: Float
    var y: Float
    /** Normalize this Vector. */
    fun normalize(): Vector2
    /** Scale this Vector by the given value. */
    fun scale(value: Float): Vector2
    fun clone(): Vector2
}
